<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('users',function (Blueprint $table) {
            $table->id();
            $table->integer('zip');
            $table->string('name');
            $table->string('ccode');
            $table->string('states');
            $table->string('password');
            $table->string('countries');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->boolean('is_admin')->default(0);
            $table->boolean('activated')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
