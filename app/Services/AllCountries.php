<?php namespace App\Services;

use PragmaRX\Countries\Package\Countries;

class AllCountries {
  
  private $countries;
  
  /**
   * Initialization of AllCountries Class.
   * @param void
   * @throws \Exception
   * @return void
   */
  public function __construct()
  {
    $this->countries = new Countries();
  }
  
  /**
   * Display All Counties resources here.
   * Normes of CC is iso_3166_1_alpha2
   * @param void
   * @throws \Exception
   * @return array
   */
  public function getAllCountries2()
  {
    return array_filter($this->countries->all()
      ->pluck('name.common','iso_3166_1_alpha2')
      ->toArray());
  }
  
  /**
   * Display All Counties resources here.
   * Norme of CC is iso_3166_1_alpha3
   * @param void
   * @throws \Exception
   * @return array
   */
  public function getAllCountries3()
  {
    return array_filter($this->countries->all()
      ->pluck('name.common','iso_3166_1_alpha3')
      ->toArray());
  }
  
  /**
   * Get the Countries matching CC.
   * @param string $name: Ex: SN
   * @throws \Exception
   * @return string
   */
  public function getCountries($name)
  {
    $states = $this->getAllCountries3();
    return $states[$name];
  }
  
  /**
   * Display States of Countries.
   * Use this function with iso_3166_1_alpha3
   * @param string $name
   * @throws \Exception
   * @return array
   */
  public function getStates($name)
  {
    return array_filter($this->countries
      ->where('cca3', (string) $name)
      ->first()->hydrateStates()->states
      ->pluck('name', 'postal')->toArray());
  }
  
}