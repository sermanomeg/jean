<?php

namespace App\Http\Controllers\Api;

use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Services\AllCountries;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class StationRegisterController extends Controller
{
	private $countries;
	
	/**
	 * Initialization of Class.
	 * function __constructor
	 * @param void
	 * @throws Exception
	 * @return void
	 */
	public function __construct()
	{
		$this->countries = new AllCountries();
	}
  
	/**
	 * Display a listing of the resource.
	 * @param void
	 * @return \Illuminate\Http\JsonResponse
	 * @throws Exception
	 */
	public function index()
	{
		$countries = $this->countries->getAllCountries3();

		return response()->json([
			'response_code'   => "00",
			'response_status' => "success",
			'response_data'   => $countries
		], 200);
	}
  
	/**
	 * Store a newly created resource in storage.
	 * @param  Request $request
	 * @throws Exception
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function store(Request $request)
	{
		$rules = [
			'states'    => ['required','string'],
			'zip'       => ['nullable','numeric'],
			'name'      => ['required','string','max:50'],
			'countries' => ['required_with:phone','string','min:2','max:3'],
			'phone'     => ['required','phone:countries','unique:users'],
			'password'  => ['required','string','min:8','max:50','confirmed'],
			'email'     => ['required','string','email:dns,spoof,filter','unique: users','max:255'],
		];

		$validated = Validator::make($request->all(),$rules);

		if ($validated->fails())
			return response()->json([
				'response_code'   => "1001",
				'response_status' => "failed",
				'response_errors' => $validated->errors()
			], 400);

		$user = User::create([
			'zip'      => $validated['zip'],
			'name'     => $validated['name'],
			'phone'    => $validated['phone'],
			'email'    => $validated['email'],
			'states'   => $validated['states'],
			'ccode'    => $validated['countries'],
			'password' => Hash::make($validated['password']),
			'countries'=> $this->countries->getCountries($validated['countries']),
		]);

		return response()->json([
			'response_code'   => "00",
			'response_status' => "success",
			'response_data'   => $user
		], 201);
	}
}
